package ares

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

// BasicQuery sends a new query to ARES
func BasicQuery(ICO string) (response EconomicEntity, err error) {
	response = EconomicEntity{}

	url := url.URL{
		Scheme: "https",
		Host:   "ares.gov.cz",
		Path:   fmt.Sprintf("ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty/%s", ICO),
	}

	r, err := http.Get(url.String())
	if err != nil {
		return
	}

	defer r.Body.Close()
	decoder := json.NewDecoder(r.Body)

	if r.StatusCode != http.StatusOK {
		aresErr := &ARESError{}
		err = decoder.Decode(&aresErr)
		if err != nil {
			return response, fmt.Errorf("HTTP Error: %s", r.Status)
		}
		return response, fmt.Errorf("ARES Error: %s; Message: %s", aresErr.Code, strings.TrimSpace(aresErr.Message))
	}

	err = decoder.Decode(&response)

	return
}

// StandardQuery allows to serach for business by its business name
func StandardQuery(name string) (responses []EconomicEntity, err error) {
	response := EconomicEntitesResponse{}
	request := EconomicEntityFilter{
		BusinessName: name,
		Limit:        100,
	}

	url := url.URL{
		Scheme: "https",
		Host:   "ares.gov.cz",
		Path:   fmt.Sprintf("ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty/vyhledat"),
	}

	body := bytes.Buffer{}
	err = json.NewEncoder(&body).Encode(request)
	if err != nil {
		return
	}

	r, err := http.Post(url.String(), "application/json", &body)
	if err != nil {
		return
	}

	defer r.Body.Close()
	decoder := json.NewDecoder(r.Body)

	if r.StatusCode != http.StatusOK {
		aresErr := &ARESError{}
		err = decoder.Decode(&aresErr)
		if err != nil {
			return []EconomicEntity{}, fmt.Errorf("HTTP Error: %s", r.Status)
		}
		return []EconomicEntity{}, fmt.Errorf("ARES Error: %s; Message: %s", aresErr.Code, strings.TrimSpace(aresErr.Message))
	}

	err = decoder.Decode(&response)

	return response.Entites, nil
}
