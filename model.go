package ares

import (
	"strconv"
	"strings"
)

type ARESError struct {
	Message string `json:"popis"`
	Code    string `json:"kod"`
	SubCode string `json:"subKod"`
}

type EconomicEntity struct {
	ICO          string  `json:"ico"`
	BusinessName string  `json:"obchodniJmeno"`
	Address      Address `json:"sidlo"`
	DIC          string  `json:"dic"`
}

type EconomicEntitesResponse struct {
	Count   int              `json:"pocetCelkem"`
	Entites []EconomicEntity `json:"ekonomickeSubjekty"`
}

type EconomicEntityFilter struct {
	Offset       int    `json:"start"`
	Limit        int    `json:"limit"`
	BusinessName string `json:"obchodniJmeno"`
}

type Address struct {
	StateCode string `json:"kodStatu"`
	State     string `json:"nazevStatu"`

	RegionCode int    `json:"kodKraje"`
	Region     string `json:"nazevKraje"`

	DistrictCode int    `json:"kodOkresu"`
	District     string `json:"nazevOkresu"`

	CityCode int    `json:"kodObce"`
	City     string `json:"nazevObce"`

	CityDistrictCode int    `json:"kodMestskeCastiObvodu"`
	CityDistrict     string `json:"nazevMestskeCastiObvodu"`

	MunicipalityPartCode int    `json:"kodCastiObce"`
	MunicipalityPart     string `json:"nazevCastiObce"`

	StreetCode int    `json:"kodUlice"`
	Street     string `json:"nazevUlice"`

	HouseNumber        int    `json:"cisloDomovni"`
	StreetNumber       int    `json:"cisloOrientacni"`
	StreetNumberLetter string `json:"cisloOrientacniPismeno"`

	PostCode    int    `json:"psc"`
	PostCodeTxt string `json:"pscTxt"`

	RUAIN int `json:"kodAdresnihoMista"`

	String string `json:"textovaAdresa"`
}

// GetAddressRows returns address formated in rows
func (a *Address) GetAddressRows() []string {
	return strings.Split(a.String, ", ")
}

// GetAddressLine returns address line
func (a *Address) GetAddressLine() string {
	return a.String
}

type SimplifiedAddress struct {
	State            string
	City             string
	CityDistrict     string
	MunicipalityPart string
	Street           string
	PostCode         string
	HouseNumber      string
	StreetNumber     string
	RUAIN            int
}

func (a *Address) ToSimplifiedAddress() SimplifiedAddress {
	simplifedAddress := SimplifiedAddress{
		State:            a.State,
		City:             a.City,
		CityDistrict:     a.CityDistrict,
		MunicipalityPart: a.MunicipalityPart,
		Street:           a.Street,
		RUAIN:            a.RUAIN,
	}

	if a.HouseNumber > 0 {
		simplifedAddress.HouseNumber = strconv.Itoa(a.HouseNumber)
	}

	if a.StreetNumber > 0 {
		simplifedAddress.StreetNumber = strconv.Itoa(a.StreetNumber)
	}

	simplifedAddress.StreetNumber += a.StreetNumberLetter

	simplifedAddress.PostCode = a.PostCodeTxt
	if a.PostCode > 0 {
		simplifedAddress.PostCode = strconv.Itoa(a.PostCode)
	}

	return simplifedAddress
}
