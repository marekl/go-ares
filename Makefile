VERSION=2.0.0

BUILDTAGS=debug

GOCMD?=$(shell which go)
GOBUILD=$(GOCMD) build
GOGENERATE=$(GOCMD) generate
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOLINT?=$(shell which golint)
GOGET=$(GOCMD) get

MODULE=go-ares
PACKAGE=gitlab.com/marekl/go-ares

ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

.PHONY: default dep build clean test coverage coverhtml lint generate

default: release

build: dep generate ## Build the binary file
	$(GOBUILD) -tags '$(BUILDTAGS)' -o bin/$(MODULE) -v $(PACKAGE)

release: BUILDTAGS=release
release: build

test: dep generate ## Run unittests
	$(GOTEST) -short $(PACKAGE)/...

race: dep generate ## Run data race detector
	$(GOTEST) -race -short $(PACKAGE)/...

msan: dep generate ## Run memory sanitizer
	$(GOTEST) -msan -short $(PACKAGE)/...

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

lint: dep generate ## Lint the files
	$(GOLINT) -set_exit_status ./...

generate: dep ## Generate files from templates
	$(GOGENERATE) -tags '$(BUILDTAGS)' $(PACKAGE)/...	

install: $(BINARY)
	@install -d $(DESTDIR)$(PREFIX)/bin/
	@install -m 755 $(BINARY) $(DESTDIR)$(PREFIX)/bin/

clean: ## Remove previous build files
	$(GOCLEAN) ./...
	rm -f $(BINARY)
	rm -rf bin/* vendor

dep: ## Get the dependencies
	$(GOGET) -v -d ./...

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'